module N26IO

using PDFIO


const cN26IO_HOME = dirname(dirname(@__FILE__))
export cN26IO_HOME


"""
    N26Transaction

A transaction from a N26 statement.

# Fields
- `date::String`: The date of the transaction.
- `receiver::String`: The contact of the transaction.
- `iban::String`: The IBAN of the contact.
- `type::String`: The type of the transaction.
- `comment::String`: The comment of the transaction.
- `amount_eur::String`: The amount of the transaction in EUR.
- `amount::String`: The amount of the transaction.
- `currency::String`: The currency of the transaction.
- `exchange_rate::String`: The exchange rate of the transaction.
"""
struct N26Transaction
    date::String
    receiver::String
    iban::String
    type::String
    comment::String
    amount_eur::String
    amount::String
    currency::String
    exchange_rate::String
end
export N26Transaction


include("csv.jl")
include("pdf.jl")


end
