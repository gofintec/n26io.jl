"""
    write_csv(dst::AbstractString, transactions::Vector{N26Transaction})::Bool

Writes a list of transactions to a CSV file.

# Arguments
- `dst::AbstractString`: The path to the CSV file.
- `transactions::Vector{N26Transaction}`: The list of transactions.

# Returns
- `Bool`: `true` if the CSV file was written successfully, `false` otherwise.
"""
function write_csv(dst::AbstractString, transactions::Vector{N26Transaction})::Bool
    open(dst, "w") do io
        # Write the header
        write(io, "\"Datum\",")
        write(io, "\"Empfänger\",")
        write(io, "\"Kontonummer\",")
        write(io, "\"Transaktionstyp\",")
        write(io, "\"Verwendungszweck\",")
        write(io, "\"Betrag (EUR)\",")
        write(io, "\"Betrag (Fremdwährung)\",")
        write(io, "\"Fremdwährung\",")
        write(io, "\"Wechselkurs\"\n")

        # Write the transactions
        for transaction in transactions
            write(io, "\"", transaction.date, "\",")
            write(io, "\"", transaction.receiver, "\",")
            write(io, "\"", transaction.iban, "\",")
            write(io, "\"", transaction.type, "\",")
            write(io, "\"", transaction.comment, "\",")
            write(io, "\"", transaction.amount_eur, "\",")
            write(io, "\"", transaction.amount, "\",")
            write(io, "\"", transaction.currency, "\",")
            write(io, "\"", transaction.exchange_rate, "\"\n")
        end
    end

    return true
end
export write_csv


"""
    read_csv(src::AbstractString)::Vector{N26Transaction}

Reads a list of transactions from a CSV file.

# Arguments
- `src::AbstractString`: The path to the CSV file.

# Returns
- `Vector{N26Transaction}`: The list of transactions.
"""
function read_csv(src::AbstractString)::Vector{N26Transaction}
    transactions = Vector{N26Transaction}()

    # Read the CSV file
    open(src, "r") do io
        # Read the header
        readline(io)

        # Read the transactions
        for line in eachline(io)
            # Skip empty lines
            if (isempty(line))
                continue
            end

            # Split the line into its components
            fields = split(strip(line), "\",\"")

            # remove leading and trailing quotes
            fields[1] = replace(fields[1], "\"" => "")
            fields[end] = replace(fields[end], "\"" => "")

            # Add the transaction to the list
            push!(
                transactions,
                N26Transaction(
                    fields[1],
                    fields[2],
                    fields[3],
                    fields[4],
                    fields[5],
                    fields[6],
                    fields[7],
                    fields[8],
                    fields[9],
                )
            )
        end
    end

    return transactions
end
export read_csv
