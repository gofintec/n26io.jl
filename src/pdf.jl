"""
    _read_page(src::AbstractString, page::Int)::String

Reads the text from a page of a PDF file.

# Arguments
- `src::AbstractString`: The path to the PDF file.
- `page::Int`: The page number to read.

# Returns
- `String`: The text from the page.
"""
function _read_page(src::AbstractString, page::Integer)::String
    io = IOBuffer()

    # Open the PDF file, get the page, and extract the text.
    doc = pdDocOpen(src)
    page = pdDocGetPage(doc, page)
    pdPageExtractText(io, page)
    pdDocClose(doc)

    # Convert the text to a string and return it.
    text = String(take!(io))
    return text
end


"""
    _get_page_number(src::AbstractString)::Int64

Returns the number of pages in a PDF file.

# Arguments
- `src::AbstractString`: The path to the PDF file.

# Returns
- `Int64`: The number of pages in the PDF file.
"""
function _get_page_number(src::AbstractString)::Int64
    doc = pdDocOpen(src)
    npage = pdDocGetPageCount(doc)
    pdDocClose(doc)

    return npage
end


"""
    _transaction_pdf!(
        transactions::Vector{N26Transaction},
        s::Vector{T}
    )::Nothing where {T<:AbstractString}

Extracts a transaction from a string vector, which was extracted from a PDF file.

# Arguments
- `transactions::Vector{N26Transaction}`: The list of transactions.
- `s::Vector{T}`: The string vector to extract the transaction from.
"""
function _transaction_pdf!(
    transactions::Vector{N26Transaction},
    s::Vector{T}
)::Nothing where {T<:AbstractString}
    # Split the first line into its components
    first_line = split(s[1])

    # Get the date and the position of the date
    date = ""
    date_pos = 0
    for i in 1:lastindex(first_line)
        m = match(r"[0-9][0-9]\.[0-9][0-9]\.[0-9][0-9][0-9][0-9]", first_line[i])
        if (m !== nothing)
            date_fracts = split(m.match, ".")
            date = string(date_fracts[3], "-", date_fracts[2], "-", date_fracts[1])
            date_pos = i
            break
        end
    end
    if (date_pos == 0)
        @warn "No date found in $(s[1])"
    end

    # Get the receiver
    receiver = join(first_line[1:date_pos-1], " ")

    # Get the amount in EUR
    m = match(r"[+-][\.0-9]+,[0-9][0-9]", first_line[date_pos+1])
    if (m === nothing)
        @warn "No amount found in $(s[1])"
    else
        amount_eur = m.match
    end

    # Split the rest of the lines into their components and get the transaction type
    transaction_tail = join(first_line[date_pos+2:end], " ")

    iban = ""
    type = "Überweisung"
    comment = ""
    amount = ""
    currency = ""
    exchange_rate = ""

    # Handle transaction type: Lastschriften
    if (occursin("Lastschriften", transaction_tail))
        iban = split(s[2])[2]
        type = "Lastschrift"
        for i in 3:length(s)-1
            comment = string(comment, " ", strip(s[i]))
        end
    end

    # Handle transaction type: Belastungen
    if (occursin("Belastungen", transaction_tail))
        iban = split(s[2])[2]
        type = "Belastung"
        for i in 3:length(s)-1
            comment = string(comment, " ", strip(s[i]))
        end
    end

    # Handle transaction type: Gutschriften
    if (occursin("Gutschriften", transaction_tail))
        iban = split(s[2])[2]
        type = "Gutschrift"
        for i in 3:length(s)-1
            comment = string(comment, " ", strip(s[i]))
        end
    end

    # Handle transaction type: Business Mastercard
    if (occursin("Business Mastercard", transaction_tail))
        type = "MasterCard Zahlung"
        comment = "-"
        amount = amount_eur
        currency = "EUR"
        exchange_rate = "1.0"
    end

    # Handle transaction type: Überweisung (default)
    if (type == "Überweisung")
        comment = join(first_line[date_pos+2:end], " ")
    end

    # Replace the decimal separator
    amount_eur = replace(amount_eur, "." => "")
    amount_eur = replace(amount_eur, "," => ".")
    amount = replace(amount, "." => "")
    amount = replace(amount, "," => ".")

    # remove leading plus symbol
    amount_eur = replace(amount_eur, "+" => "")
    amount = replace(amount, "+" => "")

    # Add the transaction to the list
    push!(
        transactions,
        N26Transaction(
            date,
            receiver,
            iban,
            type,
            comment,
            amount_eur,
            amount,
            currency,
            exchange_rate
        )
    )

    return nothing
end


"""
    _transactions_pdf!(
        transactions::Vector{N26Transaction},
        s::Vector{T}
    )::Nothing where {T<:AbstractString}

Extracts transactions from a string vector, which was extracted from a PDF file.

# Arguments
- `transactions::Vector{N26Transaction}`: The list of transactions.
- `s::Vector{T}`: The string vector to extract the transactions from.
"""
function _transactions_pdf!(transactions::Vector{N26Transaction}, s::Vector{T})::Nothing where {T<:AbstractString}
    next = 1
    for i in 1:length(s)
        if (!isempty(s[i]))
            continue
        end
        if (next < i)
            _transaction_pdf!(transactions, s[next:i-1])
        end
        next = i + 1
    end

    return nothing
end


"""
    read_statement_pdf(
        src::AbstractString
    )::Tuple{Dict{String,Vector{N26Transaction}},String,String,Bool}

Reads a N26IO statement from a PDF file.

# Arguments
- `src::AbstractString`: The path to the PDF file.

# Returns
- `Dict{String,Vector{N26Transaction}}`: The transactions.
- `String`: The month of the statement.
- `String`: The year of the statement.
- `Bool`: `true` if the statement is preliminary, `false` otherwise.
"""
function read_statement_pdf(
    src::AbstractString
)::Tuple{Dict{String,Vector{N26Transaction}},String,String,Bool}
    transactions = Dict{String,Vector{N26Transaction}}()

    # Read the first page
    text = _read_page(src, 1)
    lines = split(text, "\n")
    header_line = split(lines[1])

    # Check if the statement is preliminary
    if (header_line[1] == "Vorläufiger")
        preliminary = 1
        is_preliminary = true
    else
        preliminary = 0
        is_preliminary = false
    end

    # Get the month and year

    date_line = split(lines[3])
    date = split(date_line[1], ".")
    month = date[2]
    year = date[3]

    # Read all pages and extract transactions
    num = _get_page_number(src)
    for i in 1:num
        text = _read_page(src, i)
        lines = split(text, "\n")
        header_line = split(lines[1])
        if (1 + preliminary < length(header_line))
            if (header_line[1+preliminary] == "Kontoauszug")
                if (!haskey(transactions, "hauptkonto"))
                    push!(transactions, "hauptkonto" => Vector{N26Transaction}())
                end
                _transactions_pdf!(transactions["hauptkonto"], lines[9:end-4])
            end
            if (header_line[1+preliminary] == "Space" && header_line[2+preliminary] == "Kontoauszug")
                key = lowercase(split(lines[3])[2])
                if (!haskey(transactions, key))
                    push!(transactions, key => Vector{N26Transaction}())
                end
                _transactions_pdf!(transactions[key], lines[10:end-4])
            end
        end
    end

    return (transactions, month, year, is_preliminary)
end
export read_statement_pdf
