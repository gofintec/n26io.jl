#!/usr/bin/env bash
#=
exec julia --color=yes --startup-file=no "${BASH_SOURCE[0]}" "$@"
=#

using N26IO


function print_help()
    println("Convert N26 PDF statements to CSV files including Spaces.")
    println("")
    println("Resulting CSV files are written to the same directory as the PDF files.")
    println("If the PDF file is named statement-<year>-<month>.pdf, the CSV files will")
    println("be named statement-<year>-<month>-<account>.csv.")
    println("")
    println("Preliminary CSV files are written to the same directory as the PDF files.")
    println("If the PDF file is named statement-<year>-<month>.pdf, the preliminary")
    println("CSV files will be named statement-<year>-<month>-pre-<account>.csv.")
    println("If the preliminiary PDF is replaced by the final PDF, the preliminary CSV")
    println("files will be moved to statement-<year>-<month>-<account>.csv.")
    println("")
    println("Arguments:")
    println("  <filename>  convert a single PDF file")
    println("  <dirname>   convert all PDF files in a directory")
    println("")
    println("Options:")
    println("  --help, -h  show this help message and exit")
    println("")
    println("Examples:")
    println("  convert.jl ~/Downloads/statement-2018-01.pdf")
    println("  convert.jl ~/Downloads")
    println("  convert.jl --help")
    println("  convert.jl -h")
    println("")
    println("Author:         Oliver Griebel <griebel+dev@mailbox.org>")
    println("License:        MIT")
    println("Documentation:  https://gofintec.gitlab.io/n26io.jl")
    println("Source code:    https://gitlab.com/gofintec/n26io.jl")
    println("Report bugs to: https://gitlab.com/gofintec/n26io.jl/-/issues")

    return
end


function convert(src::AbstractString)::Bool
    if (!endswith(src, ".pdf"))
        @warn string(src, " is not a PDF file")
        return false
    end
    if (!isfile(src))
        @warn string(src, " does not exist")
        return false
    end

    @info string("Converting ", src)

    dict, m, y, is_pre = read_statement_pdf(src)

    for (k, v) in dict
        # delete preliminary csv files, if any exists
        dst_pre = joinpath(dirname(src), "statement-$(y)-$(m)-pre-$(k).csv")
        if (isfile(dst_pre))
            rm(dst_pre)
        end

        # write csv files
        if is_pre
            dst = dst_pre
        else
            dst = joinpath(dirname(src), "statement-$(y)-$(m)-$(k).csv")
        end
        write_csv(dst, v)
    end

    return true
end


function main(args)
    if ((1 < length(args) < 2) || (args[1] == "--help") || (args[1] == "-h"))
        return print_help()
    end

    # if argument is a filename, convert it
    if (isfile(args[1]))
        return convert(args[1])
    end

    # if argument is a directory, convert all pdf files in it
    if isdir(args[1])
        success = true
        for filename in readdir(args[1])
            filename = joinpath(args[1], filename)
            if endswith(filename, ".pdf")
                success = success && convert(filename)
            end
        end
        return success
    end
end


main(ARGS)
