[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://gofintec.gitlab.io/n26io.jl/dev/)
[![pipeline status](https://gitlab.com/gofintec/n26io.jl/badges/master/pipeline.svg)](https://gitlab.com/gofintec/n26io.jl/-/commits/master)
[![coverage report](https://gitlab.com/gofintec/n26io.jl/badges/master/coverage.svg)](https://gitlab.com/gofintec/n26io.jl/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)
[![ColPrac: Contributor Guide on Collaborative Practices for Community Packages](https://img.shields.io/badge/ColPrac-Contributor%20Guide-blueviolet)](https://github.com/SciML/ColPrac)

# N26IO

**N26IO converts PDF statements to CSV files including Spaces.**

## Installation

Install with the Julia package manager [Pkg](https://pkgdocs.julialang.org/), just like any other registered Julia package:

```jl
pkg> add N26IO  # Press ']' to enter the Pkg REPL mode.
```
or

```jl
julia> using Pkg; Pkg.add("N26IO")
```

## Usage

There is a command line interface (CLI) available to convert PDF statements to CSV files.
The script is called `convert.jl`.

### Convert PDF to CSV

Run the convert script with the path to the PDF file as argument.

```bash
./convert.jl statement-2023-06.pdf
```

### Convert PDFs in directory to CSV

Run the convert script with the directory containing the PDF files as argument.

```bash
./convert.jl data/
```

### Help

Plot the help message with the `-h` flag.

```bash
./convert.jl -h
```

## Known Issues

- [ ] Only german language PDFs are supported for import.
- [ ] Test coverage is not complete due to the lack of a test PDF.

## Contributing

Contributions are very welcome, as are feature requests and suggestions. Please open an [issue](https://gitlab.com/gofintec/n26io.jl/-/issues) if you encounter any problems.

## License

This project is licensed under the [MIT LICENSE](LICENSE.md).

## Acknowledgments

- [N26](https://n26.com)

[![Buy Me a Coffe](https://www.buymeacoffee.com/assets/img/custom_images/yellow_img.png)](https://www.buymeacoffee.com/gogriebel)