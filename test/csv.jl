@testset "csv.jl" begin
    account = [
        N26Transaction(
            "2023-06-11",
            "John Doe",
            "DE12345678901234567890",
            "Gutschrift",
            "",
            "160.00",
            "",
            "",
            "",
        )
    ]

    # test write CSV file
    write_csv(joinpath(cN26IO_HOME, "test", "tmp.csv"), account)
    @test isfile(joinpath(cN26IO_HOME, "test", "tmp.csv"))
    @test isfile(joinpath(cN26IO_HOME, "test", "data", "statement-2023-06-hauptkonto.csv"))
    @test isequal(
        read(joinpath(cN26IO_HOME, "test", "tmp.csv")),
        read(joinpath(cN26IO_HOME, "test", "data", "statement-2023-06-hauptkonto.csv"))
    )
    rm(joinpath(cN26IO_HOME, "test", "tmp.csv"))

    # test read CSV file
    s = read_csv(joinpath(cN26IO_HOME, "test", "data", "statement-2023-06-hauptkonto.csv"))
    @test length(s) == length(account)
    for i in 1:lastindex(s)
        @test s[i] == account[i]
    end
end
