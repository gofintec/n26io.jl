using N26IO
using Test

@testset "N26IO.jl" begin
    include("pdf.jl")
    include("csv.jl")
end
