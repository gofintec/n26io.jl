```@meta
CurrentModule = N26IO
```

# N26IO

Documentation for [N26IO](https://gitlab.com/griebel/N26IO.jl).

```@index
```

```@autodocs
Modules = [N26IO]
```
