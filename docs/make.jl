using N26IO
using Documenter

DocMeta.setdocmeta!(N26IO, :DocTestSetup, :(using N26IO); recursive=true)

makedocs(;
    modules=[N26IO],
    authors="Oliver Griebel",
    repo="https://gitlab.com/griebel/N26IO.jl/blob/{commit}{path}#{line}",
    sitename="N26IO.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://griebel.gitlab.io/N26IO.jl",
        edit_link="master",
        assets=String[]
    ),
    pages=[
        "Home" => "index.md",
    ]
)
